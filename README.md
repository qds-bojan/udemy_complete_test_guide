# Complete guide to unit testing

You can learn from this repository how to test backend NodeJS code with mocha + jest

Learn step by step by history of commits to understand code better.

First was used Mocha, and then migrated to Jest, as you might see in commits hierarchy.

There is 3 main modules: app.js (easiest example), parse-stringify.js and users.js

## There is 3 main modules:

* [app.js] - Very simple one to start
* [parse-stringify.js] - This module handles http GET requests (i.e. // takes in ?by=JoeBlogs and returns { by: 'JoeBlogs' })
* [users.js] - Used to get user information based on its "ID" or "EMAIL"

## Technologies used

* [Jest](https://jestjs.io/) - Unit testing framework
* [Mocha](https://mochajs.org/) - Dependency Management
* [NYC](https://istanbul.js.org/docs/tutorials/mocha/) - Used to do coverages and its reports

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Coverages and reporting

You can do coverages of your tests by adding attribute "**--coverage**" after defining jest as a testing framework (check package.json for example)

```
"scripts": {
    "test": "jest --coverage"
  }
```

Because Jest comes with lots of additional functionalities,
you will also see html template of report inside root folder in **lcov-report/index.html**

### Running tests

```
npm install
npm test
```

You will see that jest is automatically triggered for testing and you will see coverage report bot in terminal and above mentioned html template folder.